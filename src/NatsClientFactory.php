<?php

declare(strict_types=1);

namespace Drupal\nats;

use Drupal\Core\Site\Settings;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * NATS client factory.
 */
class NatsClientFactory {

  public function __construct(
    protected readonly ContainerInterface $container,
  ) {}

  /**
   * Create NATS client with configuration for `$name`.
   */
  public function get(string $name) {
    $settings = $this->getClientSettings($name);
    if (!$settings) {
      throw new \Exception("NATS client '{$name}' is not configured in settings.php");
    }

    // Conditionally set the logger instance if set in settings
    // for this particular client.
    $logger = NULL;
    if (isset($settings['logger'])) {
      $logger = $this->container->get($settings['logger'], ContainerInterface::NULL_ON_INVALID_REFERENCE);

      if (!$logger instanceof LoggerInterface) {
        $logger = NULL;
      }

      // Remove the logger setting from the client settings.
      unset($settings['logger']);
    }

    return new WrappedNatsClient($name, $settings, $logger);
  }

  /**
   * Get client settings from settings.php.
   */
  protected function getClientSettings(string $name) {
    $settings = Settings::get('nats');
    if (!$settings) {
      return NULL;
    }

    // Check existence of client settings.
    if (!array_key_exists($name, $settings)) {
      return NULL;
    }

    $clientSettings = $settings[$name];
    if (!is_array($clientSettings)) {
      return NULL;
    }

    return array_merge($this->getDefaultSettings(), $clientSettings);
  }

  /**
   * Default client settings.
   */
  protected function getDefaultSettings(): array {
    return [
      'jwt' => NULL,
      'pass' => NULL,
      'lang' => 'php',
      'port' => 4222,
      'reconnect' => TRUE,
      'timeout' => 1,
      'pedantic' => FALSE,
      'token' => NULL,
      'user' => NULL,
      'nkey' => NULL,
      'verbose' => FALSE,
      'version' => 'dev',
    ];
  }

}
