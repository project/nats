<?php

declare(strict_types=1);

namespace Drupal\nats;

use Basis\Nats\Client;

/**
 * Interface for a wrapped NATS client.
 */
interface WrappedNatsClientInterface {

  /**
   * Get the underlying NATS client.
   */
  public function getClient(): Client;

  /**
   * Get name of this client instance.
   */
  public function getName(): string;

}
