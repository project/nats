<?php

declare(strict_types=1);

namespace Drupal\nats;

use Basis\Nats\Client;
use Basis\Nats\Configuration;
use Psr\Log\LoggerInterface;

/**
 * Wrapped NATS client.
 */
class WrappedNatsClient implements WrappedNatsClientInterface {

  /**
   * Underlying NATS client.
   */
  protected Client $client;

  /**
   * Construct new instance.
   */
  public function __construct(
    protected readonly string $name,
    protected readonly array $settings,
    protected readonly ?LoggerInterface $logger,
  ) {
    $configuration = new Configuration($settings);
    $configuration->setDelay(0.001);

    $this->client = new Client($configuration, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public function getClient(): Client {
    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->name;
  }

}
