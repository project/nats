<?php

declare(strict_types=1);

namespace Drupal\nats;

use Basis\Nats\Client;

/**
 * NATS client manager.
 */
class NatsClientManager implements NatsClientManagerInterface {

  /**
   * Array of registered clients.
   *
   * @var WrappedNatsClientInterface[]
   */
  protected $clients = [];

  /**
   * {@inheritdoc}
   */
  public function addClient(WrappedNatsClientInterface $client): void {
    $this->clients[$client->getName()] = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $name = 'default'): ?Client {
    foreach ($this->clients as $wrappedClient) {
      if ($wrappedClient->getName() === $name) {
        return $wrappedClient->getClient();
      }
    }

    if (isset($this->clients['default'])) {
      return $this->clients['default']->getClient();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function checkHealth(): array {
    $status = [];

    foreach ($this->clients as $wrappedClient) {
      $client = $wrappedClient->getClient();

      try {
        $result = $client->ping();
        $status[$wrappedClient->getName()] = $result;
      }
      catch (\Exception) {
        $status[$wrappedClient->getName()] = FALSE;
      }
    }

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableClients(): array {
    return array_keys($this->clients);
  }

}
