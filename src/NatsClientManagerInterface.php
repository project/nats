<?php

declare(strict_types=1);

namespace Drupal\nats;

use Basis\Nats\Client;

/**
 * Interface for NATS client manager.
 */
interface NatsClientManagerInterface {

  /**
   * Add client in manager.
   *
   * @param \Drupal\nats\WrappedNatsClientInterface $client
   *   The NATS client to add.
   */
  public function addClient(WrappedNatsClientInterface $client): void;

  /**
   * Get client by optional name.
   *
   * Returns default client if specific one not found,
   * or not specified.
   *
   * @param string $name
   *   The name of the client to get. Defaults to 'default'.
   *
   * @return \Basis\Nats\Client|null
   *   The NATS client instance or NULL if not found.
   */
  public function get(string $name = 'default'): ?Client;

  /**
   * Check connection to each registered client.
   *
   * @return array
   *   Array of connection status for each client, keyed by client name.
   */
  public function checkHealth(): array;

  /**
   * Get all available clients.
   *
   * @return array
   *   Array of keys of available clients.
   */
  public function getAvailableClients(): array;

}
