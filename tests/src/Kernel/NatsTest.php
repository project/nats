<?php

declare(strict_types=1);

namespace Drupal\Tests\nats\Kernel;

use Basis\Nats\Client;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Nats test.
 */
class NatsTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'nats',
  ];

  /**
   * Throw exception if client is not configured.
   */
  public function testMissingSettings() {
    $this->expectExceptionMessage("NATS client 'default' is not configured in settings.php");

    $this->container->get('nats.client_manager');
  }

  /**
   * Test client.
   */
  public function testClient() {
    $this->setSetting('nats', [
      'default' => [
        'host' => 'localhost',
        'port' => 4222,
      ],
    ]);

    /** @var \Drupal\nats\NatsClientManager $manager */
    $manager = $this->container->get('nats.client_manager');

    $this->assertInstanceOf(Client::class, $manager->get('default'));
  }

  /**
   * Test manager.
   */
  public function testManager() {
    $this->setSetting('nats', [
      'default' => [
        'host' => 'localhost',
        'port' => 4222,
      ],
    ]);

    /** @var \Drupal\nats\NatsClientManager $manager */
    $manager = $this->container->get('nats.client_manager');

    $result = $manager->checkHealth();

    $this->assertFalse($result['default']);
  }

}
