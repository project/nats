<?php

declare(strict_types=1);

namespace Drupal\Tests\nats\Kernel;

use Basis\Nats\Client;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Nats client test.
 */
class NatsClientTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'nats',
    'nats_test',
  ];

  /**
   * Test client.
   */
  public function testClient() {
    $this->setSetting('nats', [
      'default' => [
        'host' => 'localhost',
        'port' => 4222,
      ],
      'test' => [
        'host' => 'localhost',
        'port' => 4222,
      ],
    ]);

    /** @var \Drupal\nats\NatsClientManager $manager */
    $manager = $this->container->get('nats.client_manager');

    $this->assertInstanceOf(Client::class, $manager->get('test'));
  }

  /**
   * Test client keys.
   */
  public function testGetAvailableClients() {
    $this->setSetting('nats', [
      'default' => [
        'host' => 'localhost',
        'port' => 4222,
      ],
      'test' => [
        'host' => 'localhost',
        'port' => 4222,
      ],
    ]);

    /** @var \Drupal\nats\NatsClientManager $manager */
    $manager = $this->container->get('nats.client_manager');

    $this->assertEquals(['default', 'test'], $manager->getAvailableClients());
  }

}
