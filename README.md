# Drupal NATS Integration

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

This module provides a fundamental integration to [NATS](https://nats.io).
Only the fundamental integration is provided, no additional features. Specific
functionality is provided by other modules.

A service is provided to get a client instance to communicate with the NATS
server. Heavy lifting is done by the awesome [nats.php](https://github.com/basis-company/nats.php)
library.

**Multiple clients:**

The module supports configuration for individual clients.
A `default` client comes shipped with the module, but feel free to create a new
client as a service (similar to define new logger channels.):

```yaml
services:
  nats_client.special_client:
    class: Drupal\nats\WrappedNatsClient
    factory: ['@nats_client.factory', 'get']
    # This is the name of the client. (used in settings.php).
    arguments: [special_client]
    tags:
      - { name: nats_client }
```

### Using the client

Use the factory to get a client by name.
Refer to the docs of [basis-company/nats](https://github.com/basis-company/nats.php)
for more info about how to use the client.

```php
/** @var Basis\Nats\Client $client */
$client = \Drupal::service('nats.client_manager')->get('default');

$client->publish('some-subject', [
    'data' => 'some-data',
]);
```

## Requirements

The core module requires Drupal 10.3 / 11 and the following composer
dependencies:

- [basis-company/nats](https://github.com/basis-company/nats.php)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### Ecosystem

Other modules build on top of this module.
See here to find out more: [drupal/nats ecosystem](https://www.drupal.org/project/nats/ecosystem)

## Configuration

Configuration is done via `settings.php`:

```php
// The following config is for the `default` client.
// Just add config with the correct client id for other clients.

// Host to NATS server.
$settings['nats']['default']['host'] = 'nats.example.com';
// Port of NATS server.
$settings['nats']['default']['port'] = 4222;
// Username.
$settings['nats']['default']['user'] = 'user';
// Password.
$settings['nats']['default']['user'] = 'password';
```

The settings are mapped 1:1 to the config array of `Basis\Nats\Configuration`.
See [nats.php docs](https://github.com/basis-company/nats.php?tab=readme-ov-file#connection)
for more info.

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
